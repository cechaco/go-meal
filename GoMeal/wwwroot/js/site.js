﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.
// Author: Chris Chacon
// Date 3/5/2023
// This Site.js is used to create custome functions to add dynamic functionality to the views

// This function opens the cart.cshtml view
function loadCart() {

    window.open("Cart");
}

// This function opens the cart.cshtml view
function closeWindow() {
    window.close("Cart");
}




/*  Function:       submitOder
 *  Description:    This function will get all the oder quantities and update the database
 *                  It will do a validity check to make sure the item is a number
 * */
function submitOrder() {
    //Customer_id = 0 is the guest account TG
    var customer_id = 0;//// --------------------------------------need to change this when we get a login customer 
    // These variables are used to hold the quantities
    //based on their item number in the database
    // Used the .value because its a text box
    var q1 = parseInt(document.getElementById("1").value);
    var q2 = parseInt(document.getElementById("2").value);
    var q3 = parseInt(document.getElementById("3").value);
    var q4 = parseInt(document.getElementById("4").value);
    var q5 = parseInt(document.getElementById("5").value);
    var q6 = parseInt(document.getElementById("6").value);
    var q7 = parseInt(document.getElementById("7").value);
    var q8 = parseInt(document.getElementById("8").value);
    var q9 = parseInt(document.getElementById("9").value);
    var q10 = parseInt(document.getElementById("10").value);
    var q11 = parseInt(document.getElementById("11").value);
    var q12 = parseInt(document.getElementById("12").value);
    var q13 = parseInt(document.getElementById("13").value);
    var q14 = parseInt(document.getElementById("14").value);
    var q15 = parseInt(document.getElementById("15").value);
    var q16 = parseInt(document.getElementById("16").value);
    var q17 = parseInt(document.getElementById("17").value);
    var q18 = parseInt(document.getElementById("18").value);
    var q19 = parseInt(document.getElementById("19").value);
    var q20 = parseInt(document.getElementById("20").value);
    var q21 = parseInt(document.getElementById("21").value);
    var q22 = parseInt(document.getElementById("22").value);
    var q23 = parseInt(document.getElementById("23").value);



    //Onhand Quantity "oq" from database.
    //These values are from the hidden HTML that hold the values
    //when the menu page gets loaded. 
    var oq1 = parseInt(document.getElementById("q1").innerHTML);
    var oq2 = parseInt(document.getElementById("q2").innerHTML);
    var oq3 = parseInt(document.getElementById("q3").innerHTML);
    var oq4 = parseInt(document.getElementById("q4").innerHTML);
    var oq5 = parseInt(document.getElementById("q5").innerHTML);
    var oq6 = parseInt(document.getElementById("q6").innerHTML);
    var oq7 = parseInt(document.getElementById("q7").innerHTML);
    var oq8 = parseInt(document.getElementById("q8").innerHTML);
    var oq9 = parseInt(document.getElementById("q9").innerHTML);
    var oq10 = parseInt(document.getElementById("q10").innerHTML);
    var oq11 = parseInt(document.getElementById("q11").innerHTML);
    var oq12 = parseInt(document.getElementById("q12").innerHTML);
    var oq13 = parseInt(document.getElementById("q13").innerHTML);
    var oq14 = parseInt(document.getElementById("q14").innerHTML);
    var oq15 = parseInt(document.getElementById("q15").innerHTML);
    var oq16 = parseInt(document.getElementById("q16").innerHTML);
    var oq17 = parseInt(document.getElementById("q17").innerHTML);
    var oq18 = parseInt(document.getElementById("q18").innerHTML);
    var oq19 = parseInt(document.getElementById("q19").innerHTML);

    //This checks to see if they entered a number in the quantity boxes
    if (isNaN(q1) || isNaN(q2) || isNaN(q3) || isNaN(q4) || isNaN(q5) || isNaN(q6) || isNaN(q7) || isNaN(q8)
        || isNaN(q9) || isNaN(q10) || isNaN(q11) || isNaN(q12) || isNaN(q13) || isNaN(q14) || isNaN(q15) || isNaN(q16)
        || isNaN(q17) || isNaN(q18) || isNaN(q19) || isNaN(q20) || isNaN(q21) || isNaN(q22) || isNaN(q23)) {
        alert("Please enter a number.");
        return;
    }
   // calculates the new quantity number for the data base
   // uses the variable to store calculated qty
   // -----------Do an if statement to see if it results in a negative 
   // ----- alert the user to change their order qty 
    var nq1 = oq1 - q1; 
    var nq2 = oq2 - q2;
    var nq3 = oq3 - q3; 
    var nq4 = oq4 - q4; 
    var nq5 = oq5 - q5; 
    var nq6 = oq6 - q6; 
    var nq7 = oq7 - q7; 
    var nq8 = oq8 - q8; 
    var nq9 = oq9 - q9; 
    var nq10 = oq10 - q10; 
    var nq11 = oq11 - q11; 
    var nq12 = oq12 - q12; 
    var nq13 = oq13 - q13; 
    var nq14 = oq14 - q14; 
    var nq15 = oq15 - q15; 
    var nq16 = oq16 - q16; 
    var nq17 = oq17 - q17; 
    var nq18 = oq18 - q18; 
    var nq19 = oq19 - q19; 

    // updates the hidden elements on menu.cshtml
    document.getElementById("q1").innerHTML = nq1;
    document.getElementById("q2").innerHTML = nq2;
    document.getElementById("q3").innerHTML = nq3;
    document.getElementById("q4").innerHTML = nq4;
    document.getElementById("q5").innerHTML = nq5;
    document.getElementById("q6").innerHTML = nq6;
    document.getElementById("q7").innerHTML = nq7;
    document.getElementById("q8").innerHTML = nq8;
    document.getElementById("q9").innerHTML = nq9;
    document.getElementById("q10").innerHTML = nq10;
    document.getElementById("q11").innerHTML = nq11;
    document.getElementById("q12").innerHTML = nq12;
    document.getElementById("q13").innerHTML = nq13;
    document.getElementById("q14").innerHTML = nq14;
    document.getElementById("q15").innerHTML = nq15;
    document.getElementById("q16").innerHTML = nq16;
    document.getElementById("q17").innerHTML = nq17;
    document.getElementById("q18").innerHTML = nq18;
    document.getElementById("q19").innerHTML = nq19;

    // Changes the item quantity box to 0 after the
    //submit button is pressed
    //document.getElementById("1").value = 0;
    //document.getElementById("2").value = 0;
    //document.getElementById("3").value = 0;
    //document.getElementById("4").value = 0;
    //document.getElementById("5").value = 0;
    //document.getElementById("6").value = 0;
    //document.getElementById("7").value = 0;
    //document.getElementById("8").value = 0;
    //document.getElementById("9").value = 0;
    //document.getElementById("10").value = 0;
    //document.getElementById("11").value = 0;
    //document.getElementById("12").value = 0;
    //document.getElementById("13").value = 0;
    //document.getElementById("14").value = 0;
    //document.getElementById("15").value = 0;
    //document.getElementById("16").value = 0;
    //document.getElementById("17").value = 0;
    //document.getElementById("18").value = 0;
    //document.getElementById("19").value = 0;
    //document.getElementById("20").value = 0;
    //document.getElementById("21").value = 0;
    //document.getElementById("22").value = 0;
    //document.getElementById("23").value = 0;
    // This is the update database query that will update the database based on what
    // items are ordered
    var query = "update menu set item_qty = CASE item_id when 1 then '" + nq1 + "' when 2 then '" + nq2 +
        "' when 3 then '" + nq3 + "' when 4 then '" + nq4 + "' when 5 then '" + nq5 + "' when 6 then '" + nq6 +
        "' when 7 then '" + nq7 + "' when 8 then '" + nq8 + "' when 9 then '" + nq9 + "' when 10 then '" + nq10 +
        "' when 11 then '" + nq11 + "' when 12 then '" + nq12 + "' when 13 then '" + nq13 + "' when 14 then '" + nq14 +
        "' when 15 then '" + nq15 + "' when 16 then '" + nq16 + "' when 17 then '" + nq17 + "' when 18 then '" + nq18 +
        "' when 19 then '" + nq19 + "' END where item_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19) ";
    var thanks = false;
    $.ajax({
        url: "/Home/UpdateDB",
        datatype: "string",
        type: "POST",
        data: {
            query: query, customer_id: customer_id, q1: q1, q2: q2, q3: q3, q4: q4, q5: q5, q6: q6, q7: q7, q8: q8, q9: q9, q10: q10,
            q11: q11, q12: q12, q13: q13, q14: q14, q15: q15, q16: q16, q17: q17, q18: q18, q19: q19,
            q20: q20, q21: q21, q22: q22, q23: q23, thanks: thanks },
        success: function (data) {

            try {
                var mydata = data;
               // alert("Order is being processed!");
              // window.open("Cart","_self");
                window.open("Cart");

                
            } catch (e) {
                alert("cant get data");
            }
        }
    });

   // sendEmail(order);
    return;
}



/*  Function:       FinalSubmit
 *  Description:    This function will get all the oder quantities and update the database
 *                  It will do a validity check to make sure the item is a number
 * */
function finalSubmit() {
    window.open("Thanks","_self");
    window.close("Menu");

    $.ajax({
        url: "/Home/Thanks",
        datatype: "string",
        type: "GET",
       
    });

    window.close("Cart");
 
 
    // sendEmail(order);
    return;
}



/*  Function:       Email
 *  Description:    This function will send an email to business with the order
 *                  the order comes from the submitOrder function
 * BIG NOTE::::::   Only works if we have a domain name!!!!!!
 * */
function sendEmail(order) {


        //// working need to add the order
    Email.send({
        Host: "smtp.gmail.com",
        Username: "onchaco@gmail.com",
        Password: "C*****",
        To: 'eugenechacon@yahoo.com',
        From: "onchaco@gmail.com",
        Subject: "Order from Gomeal",
        Body:  order ,
    })
        .then(function (message) {
           // alert("mail sent successfully")
        });
    
   return;
}

/*  Function:       bigPic and closePic
 *  Description:    The next blocks of code are unhide the photos and expand the photos into their own window
 *                  The closePic will hide the html element again
 * */

function bigPic(img) {
    //var big = document.getElementById("img1");
    var big = document.getElementById(img);
    big.style.display = "block";
    big.preventDefault();
    return false;
    return;
}

function closePic(img) {
    var big = document.getElementById(img);
    big.style.display = "none";
    return;
}

/*the following function will populate the hidden item quantities on the menu page*/
function GetMenuQty() {
    // uses an ajax to get the data from the query in the home controller
     //alert("from the load");
    $.ajax({
        url: "/Home/GetMenuQty",
        type: "GET",
        success: function (data) {
            var mydata;
            try {
                // we have to reference it as an array
                mydata = JSON.parse(data);
            
                var n1 = mydata[0].qty;
                var n2 = mydata[1].qty;
                var n3 = mydata[2].qty;
                var n4 = mydata[3].qty;
                var n5 = mydata[4].qty;
                var n6 = mydata[5].qty;
                var n7 = mydata[6].qty;
                var n8 = mydata[7].qty;
                var n9 = mydata[8].qty;
                var n10 = mydata[9].qty;
                var n11 = mydata[10].qty;
                var n12 = mydata[11].qty;
                var n13 = mydata[12].qty;
                var n14 = mydata[13].qty;
                var n15 = mydata[14].qty;
                var n16 = mydata[15].qty;
                var n17 = mydata[16].qty;
                var n18 = mydata[17].qty;
                var n19 = mydata[18].qty;
          
                document.getElementById("q1").innerHTML = n1;
                document.getElementById("q2").innerHTML = n2;
                document.getElementById("q3").innerHTML = n3;
                document.getElementById("q4").innerHTML = n4;
                document.getElementById("q5").innerHTML = n5;
                document.getElementById("q6").innerHTML = n6;
                document.getElementById("q7").innerHTML = n7;
                document.getElementById("q8").innerHTML = n8;
                document.getElementById("q9").innerHTML = n9;
                document.getElementById("q10").innerHTML = n10;
                document.getElementById("q11").innerHTML = n11;
                document.getElementById("q12").innerHTML = n12;
                document.getElementById("q13").innerHTML = n13;
                document.getElementById("q14").innerHTML = n14;
                document.getElementById("q15").innerHTML = n15;
                document.getElementById("q16").innerHTML = n16;
                document.getElementById("q17").innerHTML = n17;
                document.getElementById("q18").innerHTML = n18;
                document.getElementById("q19").innerHTML = n19;

                if (n1 < 10) {
                 //   document.getElementById("1").setAttribute("type", "text");
                    document.getElementById("1").value = "000000";
                    document.getElementById("1").readOnly = true;
                }
                if (n2 < 10) {
                  //  document.getElementById("2").setAttribute("type", "text");
                    document.getElementById("2").value = "000000";
                    document.getElementById("2").readOnly = true;
                }
                if (n3 < 10) {
                  //  document.getElementById("3").setAttribute("type", "text");
                    document.getElementById("3").value = "000000";
                    document.getElementById("3").readOnly = true;
                }
                if (n4 < 10) {
                //    document.getElementById("4").setAttribute("type", "text");
                    document.getElementById("4").value = "000000";
                    document.getElementById("4").readOnly = true;
                }
                if (n5 < 10) {
                //    document.getElementById("5").setAttribute("type", "text");
                    document.getElementById("5").value = "000000";
                    document.getElementById("5").readOnly = true;
                }
                if (n6 < 10) {
                //    document.getElementById("6").setAttribute("type", "text");
                    document.getElementById("6").value = "000000";
                    document.getElementById("6").readOnly = true;
                }
                if (n7 < 10) {
                //    document.getElementById("7").setAttribute("type", "text");
                    document.getElementById("7").value = "000000";
                    document.getElementById("7").readOnly = true;
                }
                if (n8 < 10) {
                //    document.getElementById("8").setAttribute("type", "text");
                    document.getElementById("8").value = "000000";
                    document.getElementById("8").readOnly = true;
                }
                if (n9 < 10) {
                //    document.getElementById("9").setAttribute("type", "text");
                    document.getElementById("9").value = "000000";
                    document.getElementById("9").readOnly = true;
                }
                if (n10 < 10) {
                 //   document.getElementById("10").setAttribute("type", "text");
                    document.getElementById("10").value = "000000";
                    document.getElementById("10").readOnly = true;
                }
                if (n11 < 10) {
                //    document.getElementById("11").setAttribute("type", "text");
                    document.getElementById("11").value = "000000";
                    document.getElementById("11").readOnly = true;
                }
                if (n12 < 10) {
                //    document.getElementById("12").setAttribute("type", "text");
                    document.getElementById("12").value = "000000";
                    document.getElementById("12").readOnly = true;
                }
                if (n13 < 10) {
                //    document.getElementById("13").setAttribute("type", "text");
                    document.getElementById("13").value = "000000";
                    document.getElementById("13").readOnly = true;
                }
                if (n14 < 10) {
                //    document.getElementById("14").setAttribute("type", "text");
                    document.getElementById("14").value = "000000";
                    document.getElementById("14").readOnly = true;
                }
                if (n15 < 10) {
                //    document.getElementById("15").setAttribute("type", "text");
                    document.getElementById("15").value = "000000";
                    document.getElementById("15").readOnly = true;
                }
                if (n16 < 10) {
                //    document.getElementById("16").setAttribute("type", "text");
                    document.getElementById("16").value = "000000";
                    document.getElementById("16").readOnly = true;
                }
                if (n17 < 10) {
                //    document.getElementById("17").setAttribute("type", "text");
                    document.getElementById("17").value = "000000";
                    document.getElementById("17").readOnly = true;
                }
                if (n18 < 10) {
                //    document.getElementById("18").setAttribute("type", "text");
                    document.getElementById("18").value = "000000";
                    document.getElementById("18").readOnly = true;
                }
                if (n19 < 10) {
                //    document.getElementById("19").setAttribute("type", "text");
                    document.getElementById("19").value = "000000";
                    document.getElementById("19").readOnly = true;
                }








            } catch (e) {
                alert("cant get data");
            }
        }
    });
    return;
}

// This function will get the cart items from the database and
// create new html elements on the cart.cshtml view
//---------------------NOT Currently using because its being done in the Controller----
function GetCartItems() {
    $.ajax({
        url: "/Home/GetCartItem",
        type: "GET",
        success: function (data) {
            var mydata;
            try {
                // we have to reference it as an array
                mydata = JSON.parse(data);
                alert(mydata);


            } catch (e) {
                alert("cant get data");
            }


            var cart_id = mydata[0].cart_id;
            var n1 = mydata[0].i1;
       //     var n2 = mydata[2].i2;
       //     var n3 = mydata[3].i3;
       //     var n4 = mydata[4].i4;
       //     var n5 = mydata[5].i5;
       //     var n6 = mydata[6].i6;
       //     var n7 = mydata[7].i7;
       //     var n8 = mydata[8].i8;
       //     var n9 = mydata[9].i9;
       //     var n10 = mydata[10].i10;
       //     var n11 = mydata[11].i11;
       //     var n12 = mydata[12].i12;
       //     var n13 = mydata[13].i13;
       //     var n14 = mydata[14].i14;
       //     var n15 = mydata[15].i15;
       //     var n16 = mydata[16].i16;
       //     var n17 = mydata[17].i17;
       //     var n18 = mydata[18].i18;
       //     var n19 = mydata[19].i19;
       //     var n20 = mydata[20].i20;
       //     var n21 = mydata[21].i21;
       //     var n22 = mydata[22].i22;
       //     var n23 = mydata[23].i23;
            alert(cart_id);
            alert(n1);
            if (n1 > 0) {
                alert("in the n1 node");
                //const element = document.createElement(htmlTag);
                //const e = document.createElement('div');
                //e.innerHTML = (n1 + " Arroz con Leche");
                //document.body.appendChild(e);
            } else {
                alert("didn't work");
            }

        }
    });


}