﻿using GoMeal.Models;
using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics.Eventing.Reader;
using System.Security.Cryptography;
using System.Text;
using System.Security;

namespace GoMeal.Controllers
{
    public class LogIn : Controller
    {
        private IHttpContextAccessor _contextAccessor;

        private const string conn_string = @"DataSource=c:\gomeal\gomeal.db;Version=3;";

        //The following coding taken from https://www.aspsnippets.com/Articles/ASPNet-Core-MVC-Encrypt-and-Decrypt-Username-or-Password-stored-in-database.aspx

        private IConfiguration Configuration;

        public string status;

        public LogIn(IHttpContextAccessor contextAccessor, IConfiguration _configuration)
        {
            _contextAccessor = contextAccessor;
            Configuration = _configuration;
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Person person)
        {
            if (!ModelState.IsValid)
            {
                return (View());
            }

            //checks if user exists - may need to use getusers
            string query1 = "SELECT EMAIL, PASSWORD FROM CUSTOMER where email = @email and password = @password";
            SQLiteConnection con1 = new SQLiteConnection(conn_string);

            SQLiteCommand cmd1 = new SQLiteCommand(query1, con1);

            cmd1.Connection = con1;
            cmd1.Parameters.AddWithValue("@email", person.Email);
            cmd1.Parameters.AddWithValue("@password", Encrypt(person.Password));
            //cmd1.Parameters.AddWithValue("@Password", person.Password);
            con1.Open();
            //cmd.ExecuteNonQuery();

            SQLiteDataReader sdr = cmd1.ExecuteReader();

            //if user exists reject registration
            if (!sdr.HasRows)
            {

                //validate the user doesn't exist before proceeding
                string query = "INSERT INTO CUSTOMER (first_name, last_name, email, password) VALUES (@first_name, @last_name, @email, @password)";
                SQLiteConnection con = new SQLiteConnection(conn_string);

                using (SQLiteCommand cmd = new SQLiteCommand(query, con))
                {
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@first_name", person.First_Name);
                    cmd.Parameters.AddWithValue("@last_name", person.Last_Name);
                    cmd.Parameters.AddWithValue("@email", person.Email);
                    cmd.Parameters.AddWithValue("@Password", Encrypt(person.Password));
                    //cmd.Parameters.AddWithValue("@Password", person.Password);
                    con.Open();
                    ViewData["result"] = cmd.ExecuteNonQuery();

                    con.Close();
                }

                _contextAccessor.HttpContext.Session.SetString("Name", person.First_Name.ToString());
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.Clear();
                ViewData["Error"] = "User already exists, try logging in.";
            }

            return View();
        }

        public ActionResult GetUsers(Person person)
        {
            //check if the user exists
            string query = "SELECT FIRST_NAME, EMAIL, PASSWORD FROM CUSTOMER where email = @email and password = @password";
            SQLiteConnection con = new SQLiteConnection(conn_string);

            using (SQLiteCommand cmd = new SQLiteCommand(query, con))
            {
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@email", person.Email);
                cmd.Parameters.AddWithValue("@Password", Encrypt(person.Password));
                con.Open();

                SQLiteDataReader sdr = cmd.ExecuteReader();

                //if sdr returns values
                if (sdr.HasRows)
                {
                    ViewData["Error"] = "User already exists.";
                    return RedirectToAction("Register", "LogIn");
                }
                else //proceed with inserting user
                {
                    return View();
                }
                con.Close();
            }
            //return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UserModel user)
        {

            if (!ModelState.IsValid)
            {
                return (View());
            }

            string query = "SELECT first_name, EMAIL, PASSWORD FROM CUSTOMER where email = @email  and password = @password";
            SQLiteConnection con = new SQLiteConnection(conn_string);

            using (SQLiteCommand cmd = new SQLiteCommand(query, con))
            {
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@email", user.Email);
                cmd.Parameters.AddWithValue("@password", Encrypt(user.Password));
                con.Open();

                SQLiteDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    //get user name as session and posts to main page
                    _contextAccessor.HttpContext.Session.SetString("Email", user.Email);
                    _contextAccessor.HttpContext.Session.SetString("Name", sdr["first_name"].ToString());
                    return RedirectToAction("Index", "Home", ViewData["Email"]);
                }
                else
                {
                    ModelState.Clear();
                    ViewData["message"] = "User Login Details Failed!";
                }

                //directs to login if email is not found
                if (!user.Email.ToString().Equals(null))
                {
                    ViewBag.Email = "";
                    status = "1";
                    ViewData["Error"] = "User not found, please try again or register as a new user.";
                }
                else
                {
                    status = "3";
                }


                con.Close();
            }

            return View();
        }

        private string Encrypt(string clearText)
        {
            string encryptionKey = System.IO.File.ReadAllText(@"C:\gomeal\key_file.txt");

            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }

            return clearText;
        }

        private string Decrypt(string cipherText)
        {
            string encryptionKey = System.IO.File.ReadAllText(@"C:\gomeal\key_file.txt");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }

            return cipherText;
        }
    }
}