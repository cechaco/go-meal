﻿using GoMeal.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Data.SQLite;

using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Security.Cryptography;
using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;


// This file is linked to the Index.html file... the main view of the website

namespace GoMeal.Controllers
{
    public class HomeController : Controller
    {

        //This is for the connection string that we are going to use to access the 
        // Data Base.. The DB should be stored in a folde called 'gomeal' in the C drive
        private const string conn_string = @"DataSource=c:\gomeal\gomeal.db;Version=3;";


        //This action returns the view for Index
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Cart()
        {
            string query = "select cart_id, i1, i2, i3, i4, i5, i6, " +
              "i7, i8, i9, i10,i11, i12, i13, i14, i15, i16, i17, i18, " +
              "i19, i20, i21, i22, i23 from cart where cart_id  = " +
              "(select MAX(cart_id) from cart)";
            SQLiteConnection con = new SQLiteConnection(conn_string);
            con.Open();
            List<Models.Cart> carts = new List<Models.Cart>();
            //this updates the menu table for invintory 
            SQLiteCommand cmd = new SQLiteCommand(query, con);
            using (SQLiteDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())// Reads through each line in list of returned parts
                {
                    Models.Cart item = new Models.Cart();
                    item.cart_id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    item.i1 = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                    item.i2 = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                    item.i3 = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                    item.i4 = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                    item.i5 = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                    item.i6 = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                    item.i7 = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                    item.i8 = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                    item.i9 = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                    item.i10 = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                    item.i11 = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                    item.i12 = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                    item.i13 = reader.IsDBNull(13) ? 0 : reader.GetInt32(13);
                    item.i14 = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                    item.i15 = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                    item.i16 = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                    item.i17 = reader.IsDBNull(17) ? 0 : reader.GetInt32(17);
                    item.i18 = reader.IsDBNull(18) ? 0 : reader.GetInt32(18);
                    item.i19 = reader.IsDBNull(19) ? 0 : reader.GetInt32(19);
                    item.i20 = reader.IsDBNull(20) ? 0 : reader.GetInt32(20);
                    item.i21 = reader.IsDBNull(21) ? 0 : reader.GetInt32(21);
                    item.i22 = reader.IsDBNull(22) ? 0 : reader.GetInt32(22);
                    item.i23 = reader.IsDBNull(23) ? 0 : reader.GetInt32(23);
                    carts.Add(item);
                }
            }

            double total = 0.00;

            // This code adds the items the ordered to the viewbag dynamically 
            if (carts[0].i1 > 0)
            {
                ViewBag.i1 = " Arroz con Lech: " + "&emsp; &emsp;" + carts[0].i1 + "&emsp; &emsp; $5.00" + "<br />";
                total += (5.00 * carts[0].i1);
            }
            if (carts[0].i2 > 0)
            {
                ViewBag.i2 = " Carnitas:  " + "&emsp; &emsp;" + carts[0].i2 + "&emsp; &emsp; $12.00" + "<br />";
                total += (12 * carts[0].i2);
            }
            if (carts[0].i3 > 0)
            {
                ViewBag.i3 = "Chicharrons: " + "&emsp; &emsp;" + carts[0].i3 + "&emsp; &emsp; $10.00" + "<br />";
                total += (10 * carts[0].i3);
            }
            if (carts[0].i4 > 0)
            {
                ViewBag.i4 = " 2 Any Style Egg: " + "&emsp; &emsp;" + carts[0].i4 + "&emsp; &emsp; $9.00" + "<br />";
                total += (9 * carts[0].i4);
            }
            if (carts[0].i5 > 0)
            {
                ViewBag.i5 = " Huevo Mexicana: " + "&emsp; &emsp;" + carts[0].i5 + "&emsp; &emsp; $9.00" + "<br />";
                total += ( 9 * carts[0].i5);
            }
            if (carts[0].i6 > 0)
            {
                ViewBag.i6 = " Menudo: " + "&emsp; &emsp;" + carts[0].i6 + "&emsp; &emsp; $12.00" + "<br />"; 
                total += (12 * carts[0].i6);
            }
            if (carts[0].i7 > 0)
            {
                ViewBag.i7 = " Enchiladas: " + "&emsp; &emsp;" + carts[0].i7 + "&emsp; &emsp; $10.00" + "<br />";
                total += (10 * carts[0].i7) ;
            }
            if (carts[0].i8 > 0)
            {
                ViewBag.i8 = " Fajitas de Pollo: " + "&emsp; &emsp;" + carts[0].i8 + "&emsp; &emsp; $13.00" + "<br />";
                total += (13 * carts[0].i8);
            }
            if (carts[0].i9 > 0)
            {
                ViewBag.i9 = " Fajitas de Res: " + "&emsp; &emsp;" + carts[0].i9 + "&emsp; &emsp; $13.00" + "<br />";
                total += (13 * carts[0].i9);
            }
            if (carts[0].i10 > 0)
            {
                ViewBag.i10 = " 5 Galletas " + "&emsp; &emsp;" + carts[0].i10 + "&emsp; &emsp; $5.00" + "<br />";
                total += (5 * carts[0].i10);
            }
            if (carts[0].i11 > 0)
            {
                ViewBag.i11 = " Chor. Eggs: " + "&emsp; &emsp;" + carts[0].i11 + "&emsp; &emsp; $9.500" + "<br />";
                total += (9.5 * carts[0].i11);
            }
            if (carts[0].i12 > 0)
            {
                ViewBag.i12 = " Huevo Rancheros: " + "&emsp; &emsp;" + carts[0].i12 + "&emsp; &emsp; $10.50" + "<br />";
                total += (10.5  * carts[0].i12);
            }
            if (carts[0].i13 > 0)
            {
                ViewBag.i13 = " Oatmeal: " + "&emsp; &emsp;" + carts[0].i13 + "&emsp; &emsp; $9.00" + "<br />";
                total += (9 * carts[0].i13);
            }
            if (carts[0].i14 > 0)
            {
                ViewBag.i14 = " Caldo Res: " + "&emsp; &emsp;" + carts[0].i14 + "&emsp; &emsp; $12.00" + "<br />";
                total += ( 12 * carts[0].i14);
            }
            if (carts[0].i15 > 0)
            {
                ViewBag.i15 = " Pozole: " + "&emsp; &emsp;" + carts[0].i15 + "&emsp; &emsp; $12.00" + "<br />";
                total += (12 * carts[0].i15);
            }
            if (carts[0].i16 > 0)
            {
                ViewBag.i16 = " Asado: " + "&emsp; &emsp;" + carts[0].i16 + "&emsp; &emsp; $12.00" + "<br />";
                total += (12 * carts[0].i16);
            }
            if (carts[0].i17 > 0)
            {
                ViewBag.i17 = " Barbacoa: " + "&emsp; &emsp;" + carts[0].i17 + "&emsp; &emsp; $12.00" + "<br />";
                total += (12 * carts[0].i17);
            }
            if (carts[0].i18 > 0)
            {
                ViewBag.i18 = " Flautas de Res: " + "&emsp; &emsp;" + carts[0].i18 + "&emsp; &emsp; $10.00" + "<br />";
                total += (10 + carts[0].i18);
            }
            if (carts[0].i19 > 0)
            {
                ViewBag.i19 = " Pastel de 3 leches: " + "&emsp; &emsp;" + carts[0].i19 + "&emsp; &emsp; $5.00" + "<br />";
                total += (5 * carts[0].i19);
            }
            if (carts[0].i20 > 0)
            {
                ViewBag.i20 = " Cofee: " + "&emsp; &emsp;" + carts[0].i20 + "&emsp; &emsp; $3.50" + "<br />";
                total += (3.5 * carts[0].i20);
            }
            if (carts[0].i21 > 0)
            {
                ViewBag.i21 = " Ice T: " + "&emsp; &emsp;" + carts[0].i21 + "&emsp; &emsp; $3.00" + "<br />";
                total += (3 * carts[0].i21);
            }
            if (carts[0].i22 > 0)
            {
                ViewBag.i22 = " Limon: " + "&emsp; &emsp;" + carts[0].i22 + "&emsp; &emsp; $3.00" + "<br />";
                total += (3 * carts[0].i22);
            }
            if (carts[0].i23 > 0)
            {
                ViewBag.i23 = " Soft Drinks: " + "&emsp; &emsp;" + carts[0].i23 + "&emsp; &emsp; $2.50" + "<br />";
                total += (2.5 * carts[0].i23) ;
            }

            ViewBag.total = total;
            return View();
        }

        //This adds the cart view
        public ActionResult Thanks()
        {
            string cartquery = "select cart_id, i1, i2, i3, i4, i5, i6, " +
              "i7, i8, i9, i10,i11, i12, i13, i14, i15, i16, i17, i18, " +
              "i19, i20, i21, i22, i23 from cart where cart_id  = " +
              "(select MAX(cart_id) from cart)";
                       
            SQLiteConnection con = new SQLiteConnection(conn_string);
            con.Open();
            List<Models.Cart> carts = new List<Models.Cart>();
            //this updates the menu table for invintory 
            SQLiteCommand cmd = new SQLiteCommand(cartquery, con);
            using (SQLiteDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())// Reads through each line in list of returned parts
                {
                    Models.Cart item = new Models.Cart();
                    item.cart_id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    item.i1 = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                    item.i2 = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                    item.i3 = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                    item.i4 = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                    item.i5 = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                    item.i6 = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                    item.i7 = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                    item.i8 = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                    item.i9 = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                    item.i10 = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                    item.i11 = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                    item.i12 = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                    item.i13 = reader.IsDBNull(13) ? 0 : reader.GetInt32(13);
                    item.i14 = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                    item.i15 = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                    item.i16 = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                    item.i17 = reader.IsDBNull(17) ? 0 : reader.GetInt32(17);
                    item.i18 = reader.IsDBNull(18) ? 0 : reader.GetInt32(18);
                    item.i19 = reader.IsDBNull(19) ? 0 : reader.GetInt32(19);
                    item.i20 = reader.IsDBNull(20) ? 0 : reader.GetInt32(20);
                    item.i21 = reader.IsDBNull(21) ? 0 : reader.GetInt32(21);
                    item.i22 = reader.IsDBNull(22) ? 0 : reader.GetInt32(22);
                    item.i23 = reader.IsDBNull(23) ? 0 : reader.GetInt32(23);
                    carts.Add(item);
                }
            }

            List<Models.menuItem> item_list = new List<menuItem>();

            string queryget = "select item_id, item_qty from menu";

            using (SQLiteCommand mycmd = new SQLiteCommand(queryget, con))
            {
                using (SQLiteDataReader reader = mycmd.ExecuteReader())
                {
                    while (reader.Read())// Reads through each line in list of returned parts
                    {
                        Models.menuItem item = new Models.menuItem();
                        item.id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        item.qty = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        item_list.Add(item);
                    }
                }
            }
        
            int nq1 = item_list[0].qty - carts[0].i1;
            int nq2 = item_list[1].qty - carts[0].i2;
            int nq3 = item_list[2].qty - carts[0].i3;
            int nq4 = item_list[3].qty - carts[0].i4;
            int nq5 = item_list[4].qty - carts[0].i5;
            int nq6 = item_list[5].qty - carts[0].i6;
            int nq7 = item_list[6].qty - carts[0].i7;
            int nq8 = item_list[7].qty - carts[0].i8;
            int nq9 = item_list[8].qty - carts[0].i9;
            int nq10 = item_list[9].qty - carts[0].i10;
            int nq11 = item_list[10].qty - carts[0].i11;
            int nq12 = item_list[11].qty - carts[0].i12;
            int nq13 = item_list[12].qty - carts[0].i13;
            int nq14 = item_list[13].qty - carts[0].i14;
            int nq15 = item_list[14].qty - carts[0].i15;
            int nq16 = item_list[15].qty - carts[0].i16;
            int nq17 = item_list[16].qty - carts[0].i17;
            int nq18 = item_list[17].qty - carts[0].i18;
            int nq19 = item_list[18].qty - carts[0].i19;
            int nq20 = item_list[19].qty - carts[0].i20;
            int nq21 = item_list[20].qty - carts[0].i21;
            int nq22 = item_list[21].qty - carts[0].i22;
            int nq23 = item_list[22].qty - carts[0].i23;

            string query = "update menu set item_qty = CASE item_id when 1 then '" + nq1 + "' when 2 then '" + nq2 +
            "' when 3 then '" + nq3 + "' when 4 then '" + nq4 + "' when 5 then '" + nq5 + "' when 6 then '" + nq6 +
            "' when 7 then '" + nq7 + "' when 8 then '" + nq8 + "' when 9 then '" + nq9 + "' when 10 then '" + nq10 +
            "' when 11 then '" + nq11 + "' when 12 then '" + nq12 + "' when 13 then '" + nq13 + "' when 14 then '" + nq14 +
            "' when 15 then '" + nq15 + "' when 16 then '" + nq16 + "' when 17 then '" + nq17 + "' when 18 then '" + nq18 +
            "' when 19 then '" + nq19 + "' when 20 then '" + nq20 + "' when 21 then '" + nq21 + "' when 122 then '" + nq22 +
            "' when 23 then '" + nq23 +
            "' END where item_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23) ";

            SQLiteCommand finalcmd = new SQLiteCommand(query, con);
            finalcmd.ExecuteNonQuery();

            return View();
        }

        //This function will  scroll through the database and return a
        //json string with a list of objects that are in the cart meny and
        // The menu quantity

        ///-------------------add viewbag or add it using json ?????????????????????
        [HttpGet]
        public string GetCartItem()
        {
            SQLiteConnection con = new SQLiteConnection(conn_string);
            con.Open();
            List<Models.Cart> item_list = new List<Cart>();

            string query = "select cart_id, i1, i2, i3, i4, i5, i6, " +
                "i7, i8, i9, i10,i11, i12, i13, i14, i15, i16, i17, i18, " +
                "i19, i20, i21, i22, i23 from cart where cart_id  = " +
                "(select MAX(cart_id) from cart)";

            using (SQLiteCommand cmd = new SQLiteCommand(query, con))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())// Reads through each line in list of returned parts
                    {
                        Models.Cart item = new Models.Cart();
                        item.cart_id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        item.i1 = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        item.i2 = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                        item.i3 = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                        item.i4 = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
                        item.i5 = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                        item.i6 = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                        item.i7 = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                        item.i8 = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                        item.i9 = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                        item.i10 = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                        item.i11 = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                        item.i12 = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                        item.i13 = reader.IsDBNull(13) ? 0 : reader.GetInt32(13);
                        item.i14 = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                        item.i15 = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                        item.i16 = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                        item.i17 = reader.IsDBNull(17) ? 0 : reader.GetInt32(17);
                        item.i18 = reader.IsDBNull(18) ? 0 : reader.GetInt32(18);
                        item.i19 = reader.IsDBNull(19) ? 0 : reader.GetInt32(19);
                        item.i20 = reader.IsDBNull(20) ? 0 : reader.GetInt32(20);
                        item.i21 = reader.IsDBNull(21) ? 0 : reader.GetInt32(21);
                        item.i22 = reader.IsDBNull(22) ? 0 : reader.GetInt32(22);
                        item.i23 = reader.IsDBNull(23) ? 0 : reader.GetInt32(23);
                        item_list.Add(item);
                    }
                }
            }
            con.Close();
            string json_table = (JsonConvert.SerializeObject(item_list).ToString());
            return json_table;
        }


        //This function will  scroll through the database and return a
        //json string with a list of objects that contain the menu ID and
        // The menu quantity
        [HttpGet]
        public string GetMenuQty()
        {
            SQLiteConnection con = new SQLiteConnection(conn_string);
            con.Open();
            List<Models.menuItem> item_list = new List<menuItem>();

            string queryget = "select item_id, item_qty from menu";

            using (SQLiteCommand cmd = new SQLiteCommand(queryget, con))
            {
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())// Reads through each line in list of returned parts
                    {
                        Models.menuItem item = new Models.menuItem();
                        item.id = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                        item.qty = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                        item_list.Add(item);
                    }
                }
            }
            con.Close();
            string json_table = (JsonConvert.SerializeObject(item_list).ToString());
            return json_table;
        }

        //The following code will update the database when the user hits the 
        //submit button. The query is sent from site.js > SubmitOrder() function.

        [HttpPost]
        public string UpdateDB(string query, int customer_id, int q1, int q2, int q3, int q4, int q5, int q6, int q7, int q8, int q9
            , int q10, int q11, int q12, int q13, int q14, int q15, int q16, int q17, int q18, int q19
            , int q20, int q21, int q22, int q23, bool thanks)
        {
            string query2 = "insert into cart ( customer_id, i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i16, i17, i18, i19, i20, i21, i22, i23)" +
                " values ( " + customer_id + ", " + q1 + "," + q2 + ", " + q3 + ", " + q4 + ", " + q5 +
                ", " + q6 + ", " + q7 + ", " + q8 + ", " + q9 + ", " + q10 + ", " + q11 +
                ", " + q12 + ", " + q13 + ", " + q14 + ", " + q15 + ", " + q16 + ", " + q17 +
                ", " + q18 + ", " + q19 + ", " + q20 + ", " + q21 + ", " + q22 + ",  " + q23 + ")";

            string done = "done";
            SQLiteConnection con = new SQLiteConnection(conn_string);
            con.Open();

            // This executes when final checkout is complete
            if (thanks == true)
            {
                //this updates the menu table for invintory 
                SQLiteCommand cmd = new SQLiteCommand(query, con);
                cmd.ExecuteNonQuery();
            }
            //This one executes when the submit is from the menu
            if (thanks == false)
            {
                //this updates the cart table
                SQLiteCommand cmd2 = new SQLiteCommand(query2, con);
                cmd2.ExecuteNonQuery();
            }
            con.Close();
            return done;
        }

        [HttpPost]
        public void completeOrder()
        {



        }


        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public ViewResult Customer()
        {
            return View();

        }

        public IActionResult Menu()
        {
            using (SQLiteConnection con = new SQLiteConnection(conn_string))
            {
                con.Open();
                // The string is the query for the db
                string query_name = "select item_name from menu order by item_category ";
                string query_price = "select item_price from menu order by item_category  ";
                string query_description = "select item_desc from menu order by item_category ";
                string query_category = "select item_category from menu order by item_category";

                string query_beverages = "select item_name, item_desc, item_price, item_id from menu where item_category like 'Beverages'order by item_name";
                string query_breakfast = "select item_name, item_desc, item_price, item_id from menu where item_category like 'Breakfast'order by item_name";
                string query_desserts = "select item_name, item_desc, item_price, item_id from menu where item_category like 'Desserts'order by item_name";
                string query_plates = "select  item_name, item_desc, item_price, item_id from menu where item_category like 'Plates'order by item_name";
                string query_soups = "select item_name, item_desc, item_price, item_id from menu where item_category like 'Soups'order by item_name";

                // The list is used to hold all fields from the query in a list form 
                List<string> menuName_list = new List<string>();
                List<float> menuPrice_list = new List<float>();
                List<string> menuDescription_list = new List<string>();
                List<string> menuCategory_list = new List<string>();

                List<Beverages> menuBeverages_list = new List<Beverages>();
                List<Breakfast> menuBreakfast_list = new List<Breakfast>();
                List<Desserts> menuDesserts_list = new List<Desserts>();
                List<Plates> menuPlates_list = new List<Plates>();
                List<Soups> menuSoups_list = new List<Soups>();


                using (SQLiteCommand cmd_name = new SQLiteCommand(query_name, con))
                {
                    using (SQLiteDataReader reader = cmd_name.ExecuteReader())
                    {
                        while (reader.Read())// Reads through each line in list of returned parts
                        {
                            menuName_list.Add(reader.GetString(0));
                        }

                    }
                }

                using (SQLiteCommand cmd_description = new SQLiteCommand(query_description, con))
                {
                    using (SQLiteDataReader reader = cmd_description.ExecuteReader())
                    {
                        while (reader.Read())// Reads through each line in list of returned parts
                        {
                            menuDescription_list.Add(reader.GetString(0));
                        }

                    }
                }

                using (SQLiteCommand cmd_price = new SQLiteCommand(query_price, con))
                {
                    using (SQLiteDataReader reader = cmd_price.ExecuteReader())
                    {
                        while (reader.Read())// Reads through each line in list of returned parts
                        {
                            menuPrice_list.Add(reader.GetFloat(0));
                        }

                    }
                }
                using (SQLiteCommand cmd_category = new SQLiteCommand(query_category, con))
                {
                    using (SQLiteDataReader reader = cmd_category.ExecuteReader())
                    {
                        while (reader.Read())// Reads through each line in list of returned parts
                        {
                            menuCategory_list.Add(reader.GetString(0));
                        }

                    }
                }

                using (SQLiteCommand cmd_beverages = new SQLiteCommand(query_beverages, con))
                {
                    using (SQLiteDataReader reader = cmd_beverages.ExecuteReader())
                    {
                        while (reader.Read())// Reads through each line in list of returned parts
                        {
                            menuBeverages_list.Add(new Beverages()
                            {
                                Item = reader.GetString(0),
                                Description = reader.GetString(1),
                                Price = reader.GetFloat(2),
                                ID = reader.GetInt32(3)
                            });
                        }

                    }
                }

                using (SQLiteCommand cmd_breakfast = new SQLiteCommand(query_breakfast, con))
                {
                    using (SQLiteDataReader reader = cmd_breakfast.ExecuteReader())
                    {
                        while (reader.Read())// Reads through each line in list of returned parts
                        {
                            menuBreakfast_list.Add(new Breakfast()
                            {
                                Item = reader.GetString(0),
                                Description = reader.GetString(1),
                                Price = reader.GetFloat(2),
                                ID = reader.GetInt32(3)
                            });

                        }

                    }

                    using (SQLiteCommand cmd_desserts = new SQLiteCommand(query_desserts, con))
                    {
                        using (SQLiteDataReader reader = cmd_desserts.ExecuteReader())
                        {
                            while (reader.Read())// Reads through each line in list of returned parts
                            {
                                menuDesserts_list.Add(new Desserts()
                                {
                                    Item = reader.GetString(0),
                                    Description = reader.GetString(1),
                                    Price = reader.GetFloat(2),
                                    ID = reader.GetInt32(3)
                                });
                            }

                        }
                    }

                    using (SQLiteCommand cmd_plates = new SQLiteCommand(query_plates, con))
                    {
                        using (SQLiteDataReader reader = cmd_plates.ExecuteReader())
                        {
                            while (reader.Read())// Reads through each line in list of returned parts
                            {
                                menuPlates_list.Add(new Plates()
                                {
                                    Item = reader.GetString(0),
                                    Description = reader.GetString(1),
                                    Price = reader.GetFloat(2),
                                    ID = reader.GetInt32(3)
                                });
                            }

                        }
                    }

                    using (SQLiteCommand cmd_soups = new SQLiteCommand(query_soups, con))
                    {
                        using (SQLiteDataReader reader = cmd_soups.ExecuteReader())
                        {
                            while (reader.Read())// Reads through each line in list of returned parts
                            {
                                menuSoups_list.Add(new Soups()
                                {
                                    Item = reader.GetString(0),
                                    Description = reader.GetString(1),
                                    Price = reader.GetFloat(2),
                                    ID = reader.GetInt32(3)

                                });
                            }

                        }
                    }

                    ViewBag.Item = menuName_list;
                    ViewBag.Price = menuPrice_list;
                    ViewBag.Description = menuDescription_list;
                    ViewBag.Category = menuCategory_list;

                    ViewBag.Beverages = menuBeverages_list;
                    ViewBag.Breakfast = menuBreakfast_list;
                    ViewBag.Desserts = menuDesserts_list;
                    ViewBag.Plates = menuPlates_list;
                    ViewBag.Soups = menuSoups_list;

                    return View(ViewBag);

                }
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public ActionResult getMenu()
        {
            SQLiteConnection con = new SQLiteConnection(conn_string);
            con.Open();
            // The string is the query for the db
            string query_name = "select item_name from menu order by item_category  ";
            string query_price = "select item_price from menu order by item_category  ";
            string query_description = "select item_desc from menu order by item_category  ";
            string query_category = "select item_category from menu order by item_category";

            string query_beverages = "select item_name, item_desc, item_price from menu where item_category like 'Beverages'order by item_name";
            string query_breakfast = "select item_name, item_desc, item_price from menu where item_category like 'Breakfast' order by item_name";
            string query_desserts = "select item_name, item_desc, item_price from menu where item_category like 'Desserts'order by item_name";
            string query_plates = "select item_name, item_desc, item_price from menu where item_category like 'Plates'order by item_name";
            string query_soups = "select item_name, item_desc, item_price from menu where item_category like 'Soups'order by item_name";

            // The list is used to hold all fields from the query in a list form 
            List<Models.Menu> menuName_list = new List<Models.Menu>();
            List<Models.Menu> menuPrice_list = new List<Models.Menu>();
            List<Models.Menu> menuDescription_list = new List<Models.Menu>();
            List<Models.Menu> menuCategory_list = new List<Models.Menu>();

            List<Models.Beverages> menuBeverages_list = new List<Models.Beverages>();
            List<Models.Breakfast> menuBreakfast_list = new List<Models.Breakfast>();
            List<Models.Desserts> menuDesserts_list = new List<Models.Desserts>();
            List<Models.Plates> menuPlates_list = new List<Models.Plates>();
            List<Models.Soups> menuSoups_list = new List<Models.Soups>();

            using (SQLiteCommand cmd = new SQLiteCommand(query_name, con))
            {

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Models.Menu menu = new Models.Menu();
                        menu.Item = reader.IsDBNull(0) ? "" : reader.GetString(0);
                        menuName_list.Add(menu);
                    }
                }
            }

            using (SQLiteCommand cmd = new SQLiteCommand(query_description, con))
            {

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Models.Menu menu = new Models.Menu();
                        menu.Description = reader.IsDBNull(0) ? "" : reader.GetString(0);
                        menuDescription_list.Add(menu);
                    }
                }
            }

            using (SQLiteCommand cmd = new SQLiteCommand(query_price, con))
            {

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Models.Menu menu = new Models.Menu();
                        menu.Price = reader.IsDBNull(0) ? 0 : reader.GetFloat(0);
                        menuPrice_list.Add(menu);
                    }
                }
            }

            using (SQLiteCommand cmd = new SQLiteCommand(query_category, con))
            {

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Models.Menu menu = new Models.Menu();
                        menu.Category = reader.IsDBNull(0) ? "" : reader.GetString(0);
                        menuCategory_list.Add(menu);
                    }
                }
            }

            using (SQLiteCommand cmd = new SQLiteCommand(query_beverages, con))
            {

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Models.Beverages menu = new Models.Beverages();
                        menu.Item = reader.IsDBNull(0) ? "" : reader.GetString(0);
                        menu.Description = reader.IsDBNull(1) ? "" : reader.GetString(1);
                        menu.Price = reader.IsDBNull(2) ? 0 : reader.GetFloat(2);

                        menuBeverages_list.Add(menu);
                    }
                }
            }


            using (SQLiteCommand cmd = new SQLiteCommand(query_breakfast, con))
            {

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Models.Breakfast menu = new Models.Breakfast();
                        menu.Item = reader.IsDBNull(0) ? "" : reader.GetString(0);
                        menu.Description = reader.IsDBNull(1) ? "" : reader.GetString(1);
                        menu.Price = reader.IsDBNull(2) ? 0 : reader.GetFloat(2);

                        menuBreakfast_list.Add(menu);
                    }
                }
            }

            using (SQLiteCommand cmd = new SQLiteCommand(query_desserts, con))
            {

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Models.Desserts menu = new Models.Desserts();
                        menu.Item = reader.IsDBNull(0) ? "" : reader.GetString(0);
                        menu.Description = reader.IsDBNull(1) ? "" : reader.GetString(1);
                        menu.Price = reader.IsDBNull(2) ? 0 : reader.GetFloat(2);

                        menuDesserts_list.Add(menu);
                    }
                }
            }

            using (SQLiteCommand cmd = new SQLiteCommand(query_plates, con))
            {

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Models.Plates menu = new Models.Plates();
                        menu.Item = reader.IsDBNull(0) ? "" : reader.GetString(0);
                        menu.Description = reader.IsDBNull(1) ? "" : reader.GetString(1);
                        menu.Price = reader.IsDBNull(2) ? 0 : reader.GetFloat(2);

                        menuPlates_list.Add(menu);
                    }
                }
            }

            using (SQLiteCommand cmd = new SQLiteCommand(query_soups, con))
            {

                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        Models.Soups menu = new Models.Soups();
                        menu.Item = reader.IsDBNull(0) ? "" : reader.GetString(0);
                        menu.Description = reader.IsDBNull(1) ? "" : reader.GetString(1);
                        menu.Price = reader.IsDBNull(2) ? 0 : reader.GetFloat(2);

                        menuSoups_list.Add(menu);
                    }
                }
            }


            con.Close();

            string json_table = (JsonConvert.SerializeObject(menuName_list).ToString());
            return Json(json_table);
        }
    }
}

