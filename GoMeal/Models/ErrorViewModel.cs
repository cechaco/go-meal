using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations;

namespace GoMeal.Models
{
    public class ErrorViewModel
    {
        public string? RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }

    //This model is used for the menu query 
    public class Menu
    {
        public int Id { get; set; }
        public string? Item { get; set; }

        public string? Description { get; set; }

        public float Price { get; set; }

        public int Qty { get; set; }

        public string? Category { get; set; }

    }

    public class UserModel
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string DecryptedPassword { get; set; }
        public string EncryptedPassword { get; set; }
        public string EncryptedEmail { get; set; }
        public string DecryptedEmail { get; set; }
        public string customID { get; set; }

        [Required(ErrorMessage = "Please enter an email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        public string Password { get; set; }
    }

        public class Beverages
    {
        public string? Item { get; set; }
        public string? Description { get; set; }
        public float Price { get; set; }
        public float ID { get; set; }

    }

    public class Breakfast
    {
        public string? Item { get; set; }
        public string? Description { get; set; }
        public float Price { get; set; }
        public float ID { get; set; }

    }

    public class Desserts
    {
        public string? Item { get; set; }
        public string? Description { get; set; }
        public float Price { get; set; }
        public float ID { get; set; }

    }

    public class Plates
    {
        public string? Item { get; set; }
        public string? Description { get; set; }
        public float Price { get; set; }
        public float ID { get; set; }

    }

    public class Soups
    {
        public string? Item { get; set; }
        public string? Description { get; set; }
        public float Price { get; set; }
        public float ID { get; set; }

    }

   /* public class UserModel
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string DecryptedPassword { get; set; }
        public string EncryptedPassword { get; set; }
        public string EncryptedEmail { get; set; }
        public string DecryptedEmail { get; set; }
        public string customID { get; set; }








    }*/
}