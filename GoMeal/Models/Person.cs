﻿using System.ComponentModel.DataAnnotations;

namespace GoMeal.Models
{
    public class Person
    {
        [Required(ErrorMessage = "First Name is required.")]
        public string First_Name { get; set; }

        [Required(ErrorMessage = "Last Name is required.")]
        public string Last_Name { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirmation Password is required.")]
        [Compare("Password", ErrorMessage = "Password and Confirmation Password must match.")]
        public string ConfirmPassword { get; set; }
    }
}
