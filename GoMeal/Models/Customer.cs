﻿namespace GoMeal.Models
{
    public class Customer
    { 
        public string Fname { get; set; }
        public string Lname { get; set; }  
        public string Email { get; set; }
        public int Card { get; set; }
        public int Csv { get; set; }    
        public int Expday { get; set; } 
        public int Expmonth { get; set; }       

    }

 
    // This is used in conjuction with the GetMenuQty function
    // in the controller. 
  
    public class menuItem
    {
        public int id;
        public int qty;

    }

    public class Cart
    {
        public int cart_id;
        //public int customer_id { get; set; }
        public int i1;
        public int i2;
        public int i3 ;
        public int i4 ;
        public int i5;
        public int i6 ;
        public int i7 ;
        public int i8 ;
        public int i9 ;
        public int i10 ;
        public int i11 ;
        public int i12 ;
        public int i13 ;
        public int i14 ;
        public int i15 ;
        public int i16 ;
        public int i17 ;
        public int i18 ;
        public int i19 ;
        public int i20 ;
        public int i21 ;
        public int i22 ;
        public int i23 ;

    }
//    public int cart_id { get; set; }
//    //public int customer_id { get; set; }
//    public int i1;
//    public int i2 { get; set; }
//    public int i3 { get; set; }
//    public int i4 { get; set; }
//    public int i5 { get; set; }
//    public int i6 { get; set; }
//    public int i7 { get; set; }
//    public int i8 { get; set; }
//    public int i9 { get; set; }
//    public int i10 { get; set; }
//    public int i11 { get; set; }
//    public int i12 { get; set; }
//    public int i13 { get; set; }
//    public int i14 { get; set; }
//    public int i15 { get; set; }
//    public int i16 { get; set; }
//    public int i17 { get; set; }
//    public int i18 { get; set; }
//    public int i19 { get; set; }
//    public int i20 { get; set; }
//    public int i21 { get; set; }
//    public int i22 { get; set; }
//    public int i23 { get; set; }
}
